const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

// echo-server
app.get('/:name' ,(req, res) => {
  res.set({'Content-type': 'text/plain'}).send(req.params.name);
});

app.get('/encode/:password' ,(req, res) => {
  const cipher = Vigenere.Cipher('password').crypt(req.params.password);
  res.set({'Content-type': 'text/plain'}).send(cipher);
});

app.get('/decode/:password', (req, res) => {
  const decipher = Vigenere.Decipher('password').crypt(req.params.password);
  res.set({'Content-type': 'text/plain'}).send(decipher);
});

app.listen(port, () => {
  console.log('Server started on ' + port);
});

